# [3.1.0](https://gitlab.com/24thsaint/animalkingdom/compare/v3.0.0...v3.1.0) (2021-10-14)


### Features

* **dog:** implement drink feature ([0c6d218](https://gitlab.com/24thsaint/animalkingdom/commit/0c6d21889d13797f5081441a2d3b2c20187ee88b))



# [3.0.0](https://gitlab.com/24thsaint/animalkingdom/compare/v2.1.1...v3.0.0) (2021-10-14)



## [2.1.1](https://gitlab.com/24thsaint/animalkingdom/compare/v2.1.0...v2.1.1) (2021-10-14)


### Bug Fixes

* **animal:** fix return value ([fc91074](https://gitlab.com/24thsaint/animalkingdom/commit/fc910743412f874cdb83c57a4849b1b89e6a440e))



# [2.1.0](https://gitlab.com/24thsaint/animalkingdom/compare/v2.0.0...v2.1.0) (2021-10-14)


### Bug Fixes

* **animal:** implement short sleep time for animals ([086ea23](https://gitlab.com/24thsaint/animalkingdom/commit/086ea23e8b07952d75b0c6e6d89fe32e26a1ed18))



# [2.0.0](https://gitlab.com/24thsaint/animalkingdom/compare/6c1973c18e977abccd434c6535b2e7bdfc1e6b50...v2.0.0) (2021-10-14)


### Features

* **animal, package.json:** add sleep and commitizen ([6c1973c](https://gitlab.com/24thsaint/animalkingdom/commit/6c1973c18e977abccd434c6535b2e7bdfc1e6b50)), closes [#1](https://gitlab.com/24thsaint/animalkingdom/issues/1)
* **app:** add husky and initial tests ([4e4f663](https://gitlab.com/24thsaint/animalkingdom/commit/4e4f663fecf7ecbf685a1c1eb4ab96edfbf83cf7))



