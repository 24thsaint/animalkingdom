const expect = require('chai').expect
const Animal = require('../src/Animal')

describe('Animal', () => {
    describe('#makeSound', () => {
        it('Makes generic animal sound', () => {
            const animal = new Animal()
            expect(animal.makeSound()).to.equal('Generic animal sound')
        })
    })
})