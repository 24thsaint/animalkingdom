const expect = require('chai').expect
const Bird = require('../src/Bird')

describe('Bird', () => {
    describe('#hunt()', () => {
        it('Bird hunts early', () => {
            const bird = new Bird()
            expect(bird.hunt()).to.equal('the early bird catches worm')
        })
    })
})