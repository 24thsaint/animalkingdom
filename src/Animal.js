class Animal {
    constructor() {}

    makeSound() {
        return 'Generic animal sound'
    }

    hunt() {
        throw new Error('Generic method, please override.')
    }

    drink() {
	    console.log('drink using straw')
    }

    sleep() {
        return 'zz'
    }
}

module.exports = Animal
