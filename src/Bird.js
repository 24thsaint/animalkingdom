const Animal = require('./Animal')

class Bird extends Animal {
    hunt() {
        return 'the early bird catches worm'
    }
}

module.exports = Bird